# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open("README.rst") as readme_file:
    readme = readme_file.read()

requirements = ["numpy", "h5py", "matplotlib"]

test_requirements = ["pytest", "pytest-cov", "responses", "pytest-mock"]

setup(
    name="py-csml",
    author="Karin Rathsman",
    author_email="karin.rathsman@esss.se",
    description="Python package for the CSML project",
    long_description=readme,
    url="https://gitlab.esss.lu.se/ics-infrastructure/py-csml",
    license="MIT license",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    install_requires=requirements,
    test_suite="tests",
    tests_require=test_requirements,
    include_package_data=True,
    packages=find_packages(include=["csml"]),
    keywords="csml",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
)
