import h5py
import glob
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib.colors import LogNorm


class EmuData:
    """
    Class for retrieving and analysing data from the emittance measurement unit.
    Generates all data files and attributes from the repository.
    """

    # Constants
    volt2ang = 79.7 / (4.0 * 7.0) * 1e3  # Convert V[kV]/W[keV] to xp [mrad]
    mc2 = 938.2720813 * 1e3  # kV

    def _readData(self, file_name, scale=1.0, method="Scalar"):
        """
        Reads h5py file

        Parameters:
        ----------
            file_name: String
                the hdf5 file name
            scale: Float
                to scale y value
            method: String
                'Array' or 'Scalar' specifying if the data is an array or scalar

        Returns:
        --------
            data

        """

        print("Reading: {}".format(file_name))
        _data = h5py.File(file_name, "r")

        if method == "Array":
            return [[float(key), scale * _data[key][()]] for key in _data]
        else:
            return [_data[key][()] for key in _data][0] * scale

    def _getFileListFromRawData(self, power, tank, solenoid_1, solenoid_2, date, time):
        """
        Generates a list of data files from the raw data repository

        Parameters:
        ----------
            power: String
                power of format '400W'

            tank: String
                for tank directory of format 'c-tank'

            solenoid_1: String
                for solenoid setting of format 'S1-306A'

            solenoid_2: String
                for solenoid setting of format 'S2-206A'

            date: String
                for date of format '2019-04-26'

            time: String
                for time of format '20-13-42'
        Returns:
        -------
            list of files from the data repository

        """
        _dirPath = "{}/{}/EMU/{}_*/{}_{}_{}/*{}_{}*.hdf5".format(
            self.data_path, date, power, tank, solenoid_1, solenoid_2, date, time
        )
        _fileList = glob.glob(_dirPath)
        return _fileList

    def _angles(self, vplus, vminus, yscale):
        """
        Calculates the detectable angle of the incomming beam

        Parameters:
        ----------
            vplus: Array of floats
                time, positive voltage from the EMU plates

            vminus: Float Array
                time, negative voltage from the EMU plates

        Returns:
        --------
            Array with time and the angle in mrad
        """

        # Interpolate
        y = []
        j = 1
        for vpi in vplus:
            t = vpi[0]
            while j < len(vminus) and vminus[j][0] <= t:
                j += 1
            t1 = vminus[j][0]
            t0 = vminus[j - 1][0]
            vmi = (vminus[j - 1][1] * (t1 - t) + vminus[j][1] * (t - t0)) / (
                t1 - t0
            )  # interpolate to the same time as in vp
            yi = (vpi[1] - vmi) * yscale
            y.append([t, yi])

        # cut timeregion:
        delta = 0.1 * (self.zpmax - self.zpmin)
        for i in range(1, len(y) - 2):
            if (
                y[i][1] - y[i - 1][1] > delta and y[i][1] > y[i + 1][1]
            ):  # local maximum with large diff to the left => first point in scanning region
                ini = i
                break
        for i in range(len(y) - 2, 1, -1):
            if (
                y[i + 1][1] - y[i][1] > delta and y[i][1] < y[i - 1][1]
            ):  # local minima with large diff to the right => last point in scanning region
                fin = i
                break
        y = y[ini:fin][:]
        return np.array(y)

    def _pulse_with_highest_current(self, fc):
        """
        Find the pulse with highest current measured with the faraday cup.

        Parameters:
        ----------
            fc: array with pulse current.

        Returns:
        --------
            tuple (index, maxium) of highest FC current
        """
        max_pulse_index = 0
        max_pulse_current = 0
        for i in range(len(fc)):
            current = max(fc[i][1])
            if current > max_pulse_current:
                max_pulse_current = current
                max_pulse_index = i

        return (max_pulse_index, max_pulse_current)

    def _trim_time(self, array, tmin, tmax):
        """
        Delete data outside tmin and tmax

        Parameters:
        -----------
            array: array
                time as first columm

            tmin: Float
                time when measurement starts

            tmax: Float
                time when measurement ends

        Returns:
        --------
            Array between tmin and tmax

        """

        nmin = 0
        while array[nmin][0] < tmin:
            nmin += 1
        nmax = len(array) - 1
        while array[nmax][0] > tmax:
            nmax -= 1
        return array[nmin:nmax]

    def _current(self, fc, selection_width=0.5, pulse_threshold=0.1):
        """Generates the average current from a selected region in the pulse.
        Args:
            fc: Array with pulses
            selection_width: A float between 0 and 1 specifying the selection width in the pulse.
                If selection_width is 1 the chopped region will be selected.
                If selection width is 0.5 half the chopped region centred around chop middle will be selected.
                If selection_width is negative the full pulse will be selected.
            threshold: If the current in the pulse is below this threashold the current is set to zero.
        """
        (max_pulse_index, max_pulse_current) = self._pulse_with_highest_current(fc)
        chop_threshold = 0.1 * max_pulse_current
        max_pulse = fc[max_pulse_index][1]
        if selection_width > 0:
            chop_start = 0
            chop_end = len(max_pulse) - 1
            while max_pulse[chop_start] < chop_threshold:
                chop_start += 1
            while max_pulse[chop_end] < chop_threshold:
                chop_end -= 1
            self.pulse_select_start = int(
                chop_start * (1.0 + selection_width) / 2.0
                + chop_end * (1 - selection_width) / 2.0
            )
            self.pulse_select_end = int(
                chop_start * (1.0 - selection_width) / 2.0
                + chop_end * (1 + selection_width) / 2.0
            )
        else:
            self.pulse_select_start = 0
            self.pulse_select_end = len(max_pulse)

        self.pulse_threshold = pulse_threshold

        # Average over the selected pulse region
        array = []
        for fci in self.fc:
            ti = fci[0]
            sumy = 0.0
            sum1 = 0.0
            yi = fci[1]
            for n in range(self.pulse_select_start, self.pulse_select_end):
                yij = yi[n]
                sum1 += 1
                if yij > self.pulse_threshold:
                    sumy += yij
            array.append([ti, sumy / sum1])
        return np.array(array)

    def __init__(self, data_path, power, tank, solenoid_1, solenoid_2, date, time):
        """
        Constructor that
            1) reads the data from repository,
            2) calculates the angular coordinates from the voltages of the EMU
            3) extracts the spatial position of the slit from the motor data.
            4) calculates the average current from the faraday cup.

        Parameters:
        ----------
            data_path: String
                path to the Saved PVs directory in repository.

            power: String
                power of format '400W'

            tank: String
                for tank directory of format 'c-tank'

            solenoid_1: String
                for solenoid setting of format 'S1-306A'

            solenoid_2: String
                for solenoid setting of format 'S2-206A'

            date: String
                for date of format '2019-04-26'

            time: String
                for time of format '20-13-42'
        """
        self.data_path = data_path
        self.power = power
        self.tank = tank
        self.solenoid_1 = solenoid_1
        self.solenoid_2 = solenoid_2
        self.date = date
        self.time = time
        self.title = "{}_{}_{}_{}_{}_{}".format(
            power, tank, solenoid_1, solenoid_2, date, time
        )

        _files = self._getFileListFromRawData(
            power, tank, solenoid_1, solenoid_2, date, time
        )
        print("{} number of hdf5 data files found".format(len(_files)))
        # read the data from files:
        for file_name in _files:
            if "MTR.P4SP" in file_name:
                x_ini = self._readData(
                    file_name, scale=-1.0
                )  # Compensate motor direction
            if "MTR.P4EP" in file_name:
                x_fin = self._readData(
                    file_name, scale=-1.0
                )  # Compensate motor direction
            if "MTR.NPTS" in file_name:
                step_x = self._readData(file_name)
            if "Mtr.RBV" in file_name:
                self.pos = np.array(
                    self._readData(file_name, scale=-1.0, method="Array")
                )
            if "HV1:VoltR:AVG" in file_name:
                vplus = np.array(self._readData(file_name, method="Array"))
            if "HV2:VoltR:AVG" in file_name:
                vminus = np.array(self._readData(file_name, method="Array"))
            if "PROC-VoltMaxP1" in file_name:
                volt_ini = self._readData(file_name)
            if "PROC-VoltMinP1" in file_name:
                volt_fin = self._readData(file_name)
            if "PS.NPTS" in file_name:
                step_xp = self._readData(file_name)
            if "Energy" in file_name:
                self.energy = self._readData(file_name, scale=1e-3)  # keV
            if "CurrR_" in file_name:
                #  fc    = np.array([[float(key), data[key][()]] for key in data])
                fc = self._readData(file_name, method="Array")

        # Angular phase space coordinates:
        yscale = self.volt2ang / self.energy  # scale factor between voltage and angle
        self.zpmax = 2 * volt_ini * yscale
        self.zpmin = 2 * volt_fin * yscale
        self.angle = self._angles(vplus, vminus, yscale)
        self.jmax = int(step_xp + 0.1)  # number of datapoints in angle.
        self.dzp = (self.zpmax - self.zpmin) / (
            self.jmax - 1
        )  # distance between points

        # Spatial phase space coordinates
        self.zmax = x_fin
        self.zmin = x_ini
        self.imax = int(step_x + 0.1)
        self.dz = (self.zmax - self.zmin) / (self.imax - 1)

        # Pulse current:
        self.tmax = self.angle[-1][0]  # time when measurement start
        self.tmin = self.angle[0][0]  # time when measurement ends
        self.fc = self._trim_time(fc, self.tmin, self.tmax)  # remove crap...
        self.current = self._current(self.fc)

    def phase_space_density(self):
        """
        Calculates the phase space density

        Returns:
        -------

            z:  Array
                One dimensional array with the spatial coordinates

            zp: Array
                One dimensional array with the angular coordinates

            den: Array
                Two dimensional array with the density.
                Angular coordinates on the vertical axis (axis 0) and spatial coordinates on the horizontal axis (axis 1)
        """
        i = 0  # index for x
        j = 0  # index for y

        c = np.zeros((self.jmax, self.imax))  # density grid
        w = np.zeros((self.jmax, self.imax))  # grid with sampling times for x, y

        for n in range(1, len(self.current)):
            (tn, cn) = self.current[n]
            if tn >= self.tmin and tn <= self.tmax:
                # find the time index for the position at time tn:
                while i + 1 < len(self.pos) and self.pos[i + 1, 0] <= tn:
                    i += 1

                # find the time index for the angle at time tn:
                while j + 1 < len(self.angle) and self.angle[j + 1, 0] <= tn:
                    j += 1

                # add to grid
                ibin = int(
                    (self.pos[i, 1] - self.zmin + 0.5 * self.dz) / self.dz
                )  # grid coordinate in x
                jbin = int(
                    (self.angle[j, 1] - self.zpmin + 0.5 * self.dzp) / self.dzp
                )  # grid coordinate in y
                c[jbin, ibin] += cn  # add current to density grid
                w[jbin, ibin] += (
                    tn - self.current[n - 1, 0]
                )  # add time interval to time grid

        # correct for different sampling times:
        for j in range(self.jmax):
            for i in range(self.imax):
                if w[j, i] > 0:
                    c[j, i] = c[j, i] / w[j, i]

        # Normalise to get the phase space density:
        c = c / np.sum(c)

        # Calculate the coordinates.
        x = np.arange(self.imax) * self.dz + self.zmin
        y = np.arange(self.jmax) * self.dzp + self.zpmin

        return x, y, c


class PhaseSpace:
    mc2 = 938.2720813 * 1e3  # kV

    def __init__(self, z, zp, density, energy):
        """
        Constructor that:

        1) Checks the indata
        2) Calculates the rms emittance and twiss parameters.

        Parameters
        ----------
            z:  1D Array
                Spatial equidistant coordinates in mm.

            zp: 1D Array
                Angular equidistant coordinates in mrad.

            den: 2D Array
                Density matrix with angular coordinates on the vertical axis (axis 0) and spatial coordinates on the horizontal axis (axis 1).
                I.e. density[j,i] is the density at zp[j] and z[i].

            energy: Float
                Kinetic energy of the beam particles in keV
        """
        assert len(density) == len(zp), "Length not matching for den and y"
        assert len(density.T) == len(z.T), "Length not matching for den and x"
        assert (np.sum(density) - 1.0) < 1.0e-9, "density is not normalized"

        gamma = 1.0 + energy / self.mc2
        betagamma = np.sqrt(gamma ** 2 - 1.0)

        sx = np.sum(density.dot(z))
        sy = np.sum(zp.dot(density))
        sxx = np.sum(density.dot(z ** 2)) - sx ** 2
        sxy = zp.dot(density).dot(z) - sx * sy
        syy = np.sum((zp ** 2).dot(density)) - sy ** 2
        eps = np.sqrt(sxx * syy - sxy * sxy)

        self.density = density
        self.zp = zp
        self.z = z
        self.alpha = -sxy / eps
        self.beta = sxx / eps  # [m]
        self.eps = eps  # [pi mm mrad]
        self.eps_star = eps * betagamma  # normalized
        self.meanz = sx
        self.meanzp = sy
        self.sigmaz = np.sqrt(self.beta * self.eps)
        self.sigmazp = np.sqrt(self.eps * (1.0 + self.alpha ** 2) / self.beta)

        self.energy = energy

        dz = (z[-1] - z[0]) / (len(z) - 1)
        dzp = (zp[-1] - zp[0]) / (len(zp) - 1)
        self.zmin = z[0] - dz / 2.0
        self.zmax = z[-1] + dz / 2.0
        self.zpmin = zp[0] - dzp / 2.0
        self.zpmax = zp[-1] + dzp / 2.0

    def rebin(self, n_z, n_zp):
        """
        Rebin the density distribution.

        Parameters:
        -----------
            n_z:    int
                    Number of new points in spatial direction (x-axis).
                    If n_z is larger than or equal to existing number of data points or
                    if n_z is smaller than one, no binning will be made in this direction.

            n_zp:   int
                    Number of new points in angular direction (y-axis)
                    If n_zp is larger than or equal to existing number of data points or
                    if n_zp is smaller than one, no binning will be made in this direction.

                Number of coordinates for the rebinned output.

        Returns:
        --------
            emittance:  Emittance
                the rebinned data
        """
        v, den = self._rebin_rows(self.zp, self.zpmin, self.zpmax, self.density, n_zp)
        u, dent = self._rebin_rows(self.z, self.zmin, self.zpmax, den.T, n_z)
        return PhaseSpace(u, v, dent.T, self.energy)

    def _rebin_rows(self, y, ymin, ymax, a, nmax):
        """
        Rebin the rows in the matrix a

        Parameters:
        -----------
            y:  Array
                equidistant coordinates in the vertical direction

            ymin: Float
                smallest value in y (=y[0]-0.5*dy)

            ymax: Float
                largets value in y (=y[-1]+0.5*dy)

            a:  Array
                Density distribution with the same number of rows as elements in y.

            nmax:
                Number of coordinates for the rebinned output.
                If the number of coordinates is larger than in the input coordinates
                or if nmax is smaller than one, no binning will be made.

        Returns:
        --------
            u:  Array
                Rebinned Coordinates.

            b:  Array
                Rebinned density distribution.
        """

        kmax, lmax = np.shape(a)
        if nmax > 0 and nmax < kmax:
            du = (ymax - ymin) / nmax
            u = ymin + (0.5 + np.arange(nmax)) * du
            assert abs(u[nmax - 1] + 0.5 * du - ymax) < 1.0e-9
            b = np.zeros((nmax, lmax))
            n = 0
            for k in range(0, kmax):
                delta = int((k + 1) * nmax - (n + 1) * kmax)
                if delta > 0:
                    b[n, :] += (nmax - delta) / nmax * a[k, :]
                    n += 1
                    b[n, :] += delta / nmax * a[k, :]
                else:
                    b[n, :] += a[k, :]
            assert abs(np.sum(b) - 1.0) < 1.0e-9, "damn"
        else:
            u = y
            b = a
        return u, b

    def ellipse(self, stds=1.0):
        """
        Calculate ellipse parameters (for plotting).

        Parameters
        ----------
            stds: int
                number of standard deviations. Default value corresponds to rms emittance.

        Returns
        -------
            (z0, zp0): tuple of Float.
                ellipse center

            width: Float
                major axis of ellipse

            hight: Float
                minor axis of ellipse

            angle: Float
                tilt of the ellipse in degrees
            """

        s = (1.0 + self.beta ** 2 + self.alpha ** 2) / (
            2 * self.beta
        )  # same as ( (beta+gamma) / 2 )
        r = s - np.sqrt(s ** 2 - 1.0)  # ratio minor to major axis
        height = 2 * np.sqrt(stds * self.eps * r)  # minor axis
        width = height / r  # major axis
        phi = np.arctan2(-self.alpha, self.beta - r) * 180.0 / np.pi  # angle in degrees
        return (self.meanz, self.meanzp), width, height, phi

    def courant_snyder_invariant(self, z, zp):
        """
        Calculates the courant snyder invariant

        Parameters:
        ----------
            z: Float
                the spatial phase space coordinate

            zp: Float
                the angular phase space coordinate

        Returns:
        --------
            invariant: Float
                the courant_snyder parameter calculated for alpha and beta of the instance of the class.
        """

        zi = z - self.meanz
        zpj = zp - self.meanzp
        invariant = (zi ** 2 + (self.beta * zpj + self.alpha * zi) ** 2) / self.beta
        return invariant

    def hist_eps(self, maxstd):
        """
        Calculates the particle density inside elipses enclosing a phase space area of n * eps_rms.
        For a gaussian distribution this should be equal to (1-exp(-n/2)), corresponding to 39 % for n = 1.

        Parameters:
        -----------
            maxstd: int
                specifies the number of standard deviations in emittance, i.e, the number of ellipses.

        Returns:
        --------
            stds: Array of lenght maxstd
                Vector of standard deviations (n) in emittance.

            percentage: Array of lenght maxstd
                The persentage of particles in the range  n-1 <= I / eps_rms < n.

            theory: Array of length maxstd
                The theoretical percentage for gaussian distribution."""
        jmax, imax = np.shape(self.density)
        percentage = np.zeros(maxstd)
        stds = np.arange(maxstd)
        theory = (1.0 - np.exp(-0.5 * (stds + 1.0))) * 100
        for j in range(jmax):
            for i in range(imax):
                s = self.courant_snyder_invariant(self.z[i], self.zp[j]) / self.eps
                if s < maxstd:
                    n = int(s)
                    percentage[n] += self.density[j, i] * 100
        return stds, np.cumsum(percentage), theory

    def beam_profile(self):
        """
        Calculates the beam profile.
        Returns:
        ----------
            zdens: Array
                weight from measurement data.

            theoryz: Array
                weight from gaussian distribution.
        """
        zdens = self.density.sum(axis=0)
        normalizedz = (self.z - self.meanz) / self.sigmaz
        theoryz = (
            np.exp(-0.5 * normalizedz ** 2)
            / (np.sqrt(2.0 * np.pi) * self.sigmaz)
            * (self.zmax - self.zmin)
            / len(zdens)
        )
        return zdens, theoryz

    def angular_profile(self):
        """
        Calculates the angular profile.
        Returns:
        ----------
            zpdens: Array
                weight from measurement data.

            theoryzp: Array
                weight from gaussian distribution.
        """
        zpdens = self.density.sum(axis=1)
        normalizedzp = (self.zp - self.meanzp) / self.sigmazp
        theoryzp = (
            np.exp(-0.5 * normalizedzp ** 2)
            / (np.sqrt(2.0 * np.pi) * self.sigmazp)
            * (self.zpmax - self.zpmin)
            / len(zpdens)
        )
        return zpdens, theoryzp


class Plotter:
    def __init__(self, title):
        """
        Helper class to plot data from the EmuData and PhaseSpace classes

        Parameters:
        ----------
            title: String
                The suptitle in all figures
        """

        self.title = title

    def plot_emuData(self, data):
        """
        Plots data from the EmuData class

        Parameters:
        ----------
            data: EmuData

        Returns:
        -------
            fig: Figure
                Figure visualizing the position, angle, pulse current from the faraday cup
                (with threashold and time limits within puls) and integrated current of the EMU vs time.

            axes: 2D Array
                axes for angle, position, pulse current, filtered and integrated current, linear and log phase space plots.
        """
        fig, axes = plt.subplots(3, 2, figsize=(8.27, 11.69), constrained_layout=True)
        fig.suptitle(self.title, fontsize="small")
        ax0 = axes[0, 0]
        ax1 = axes[0, 1]
        ax2 = axes[1, 0]
        ax3 = axes[1, 1]
        ax4 = axes[2, 0]
        ax5 = axes[2, 1]

        # Angle
        ax0.plot(data.angle[:, 0], data.angle[:, 1])
        ax0.axis([data.tmin, data.tmax, data.zpmin, data.zpmax])
        ax0.set_xlabel("time (s)")
        ax0.set_ylabel("zp [mrad]")
        ax0.set_title("Angle")

        # Position
        ax1.plot(data.pos[:, 0], data.pos[:, 1])
        ax1.axis([data.tmin, data.tmax, data.zmin, data.zmax])
        ax1.set_xlabel("time (s)")
        ax1.set_ylabel("z (mm)")
        ax1.set_title("Position")

        # Pulses
        index_at_max, _ = data._pulse_with_highest_current(data.fc)
        max_puls = data.fc[index_at_max][1]
        other_puls = data.fc[4000][1]

        ax2.plot(max_puls)
        ax2.plot(other_puls)
        ax2.axhline(data.pulse_threshold, color="green")
        ax2.set_title("FC_current within pulse")
        ax2.set_xlabel("index")
        ax2.axvspan(
            data.pulse_select_start, data.pulse_select_end, alpha=0.5, facecolor="green"
        )

        # Current
        ax3.plot(data.current[:, 1])
        ax3.set_title("filtered and averaged current")
        ax3.set_xlabel("index")

        # Phase space linear scale
        z, zp, density = data.phase_space_density()
        density *= 100
        z = np.concatenate([z - 0.5 * data.dz, [z[-1] + 0.5 * data.dz]])
        zp = np.concatenate([zp - 0.5 * data.dzp, [zp[-1] + 0.5 * data.dzp]])

        ax4.set_title("Phase space, linar scale")
        ax4.set_xlabel("z [mm]")
        ax4.set_ylabel("z' [mrad]")
        ax4.pcolor(z, zp, density, cmap="RdBu")  # the phase space density

        # Phase space log scale
        #
        average = np.sum(density) / (len(z) * len(zp))
        vmin = average / 100
        vmax = average * 100
        ax5.set_title("Phase space, logarithmic")
        ax5.set_xlabel("z [mm]")
        ax5.pcolor(z, zp, density, cmap="RdBu", norm=LogNorm(vmin=vmin, vmax=vmax))

        return fig, axes

    def plot_pulses(self, data):
        """
        Plots all the pulscurrents during one scan with the EMU as an image with time indices on the y and x axis.

        Parameters:
        ----------
            data: EmuData

        Returns:
        -------
            fig: Figure
                Figure visualizing the fc current through each pulse in a colorplot (imshow). To be used to select filtering and threashold parameters and evaluate the quality of emittance data.

            ax: Axes
                Axes
        """
        pulses = []
        for item in data.fc:
            stupid = item[1]
            array = []
            for s in stupid:
                array.append(s)
            pulses.append(array)

        fig, ax = plt.subplots(figsize=(8.27, 11.69))
        fig.suptitle(self.title, fontsize="small")

        ax.imshow(pulses, cmap="RdBu")
        ax.set_title("pulses")
        ax.set_xlabel("time index")
        ax.set_ylabel("time index")
        return fig, ax

    def plot_emittance(self, ps, maxstd=1):
        """
        Plots the emittance, spatial and angular beam profiles and the cumulative distribution of courant snyder parameters.

        Parameters:
        ----------
            ps: PhaseSpace
                the phase space

            maxstd: Int (optional)
                integer larger than zero specifies the number of ellipses and steps in the distribution of courant snyder invariants to plot. Default is unit

        Returns:
        -------
            fig: Figure
                Figure with four axes

            axes: 2D Array
                Axes with the beam profile, histogram of emittance, phase space distribution and angular distribution
        """

        maxstd = max(1, maxstd)
        stds, percentage, theory = ps.hist_eps(maxstd)
        fig, axes = plt.subplots(2, 2, figsize=(8.27, 11.69), constrained_layout=True)
        fig.suptitle(self.title, fontsize="small")
        ax_beamprofile = axes[0, 0]
        ax_epsilon = axes[0, 1]
        ax_ellipses = axes[1, 0]
        ax_angular = axes[1, 1]

        # Ellipses and phase space distribution
        x = np.concatenate([ps.z - ps.z[0] + ps.zmin, [ps.zmax]])
        y = np.concatenate([ps.zp - ps.zp[0] + ps.zpmin, [ps.zpmax]])

        ax_ellipses.set_title(
            r"Emittance ellipses for $\epsilon = n \epsilon_\mathrm{{rms}}$"
        )
        ax_ellipses.set_xlabel(r"$z$ (mm)")
        ax_ellipses.set_ylabel(r"$z^{{\prime}}$ (mrad)")
        ax_ellipses.set_ylim([ps.zpmin, ps.zpmax])
        ax_ellipses.set_xlim([ps.zmin, ps.zmax])
        ax_ellipses.pcolor(x, y, ps.density, cmap="RdBu")

        for std in stds:
            center, width, height, phi = ps.ellipse(std + 1)
            e = Ellipse(center, width, height, phi, color="w", fill=False)
            ax_ellipses.add_artist(e)

        # Emittance (courant snyder invariant) axes
        ax_epsilon.set_title("Number of particles inside emittance ellipses")
        ax_epsilon.set_xlabel(r"$\epsilon / \epsilon_\mathrm{{rms}}$")
        ax_epsilon.set_ylabel("Number of particles (%)")
        ax_epsilon.hist(
            stds,
            range=(0, maxstd),
            bins=len(stds),
            weights=percentage,
            alpha=0.5,
            label="measured data",
        )
        ax_epsilon.hist(
            stds,
            range=(0, maxstd),
            bins=len(stds),
            weights=theory,
            histtype="step",
            label="gaussian distribution",
        )
        ax_epsilon.set_ylim([0, 100])
        ax_epsilon.legend(prop={"size": 10})
        text = (
            r"$\epsilon_\mathrm{{rms}} = ${}  $\pi$ mm mrad"
            "\n"
            r"$\epsilon^*_\mathrm{{rms}} = ${}  $\pi$ mm mrad"
            "\n"
            r"$\beta = ${} m"
            "\n"
            r"$\alpha =$ {}".format(
                np.round(ps.eps, 1),
                np.round(ps.eps_star, 3),
                np.round(ps.beta, 2),
                np.round(ps.alpha, 2),
            )
        )
        ax_epsilon.text(0.1, 5, text)
        tics = np.arange(maxstd + 1)
        ax_epsilon.set_xticks(tics)

        # Beam profile axes
        zdens, theoryz = ps.beam_profile()
        ax_beamprofile.set_title(r"Transverse beam profile")
        ax_beamprofile.set_xlabel(r"$z$ (mm)")
        ax_beamprofile.set_xlim([ps.zmin, ps.zmax])

        ax_beamprofile.set_ylabel(r"Number of particles (%)")
        ax_beamprofile.hist(
            ps.z,
            range=(ps.zmin, ps.zmax),
            bins=len(zdens),
            weights=zdens * 100,
            alpha=0.5,
            label="measured data",
        )
        ax_beamprofile.plot(ps.z, theoryz * 100, label="gaussian distribution")
        ax_beamprofile.legend(prop={"size": 10})

        # Angular axes
        zpdens, theoryzp = ps.angular_profile()
        ax_angular.set_title(r"Angular spread")
        ax_angular.set_ylabel(r"$z^{{\prime}}$ (mrad)")
        ax_angular.set_ylim([ps.zpmin, ps.zpmax])
        ax_angular.set_xlabel(r"Number of particles (%)")
        ax_angular.hist(
            ps.zp,
            range=(ps.zpmin, ps.zpmax),
            bins=len(zpdens),
            weights=zpdens * 100,
            alpha=0.5,
            label="measured data",
            orientation="horizontal",
        )
        ax_angular.plot(theoryzp * 100, ps.zp, label="gaussian distribution")
        ax_angular.legend(prop={"size": 10})
        return fig, axes
